(function() {
  var Backdrop;

  Backdrop = (function() {
    function Backdrop(options) {
      var _this = this;
      if (options == null) {
        options = {};
      }
      this._backdropClass = options.backdropClass || 'backdropBackdrop';
      this._highlightClass = options.highlightClass || 'backdropHighlight';
      this._elementClass = options.elementClass || 'backdropElement';
      this._baseZIndex = options.baseZIndex || 1000;
      this._padding = options.padding || 5;
      this._onShow = options.onShow || function() {};
      this._onHide = options.onHide || function() {};
      this._onAdd = options.onAdd || function() {};
      this._onRemove = options.onRemove || function() {};
      this._onCreate = options.onCreate || function() {};
      this._onDestroy = options.onDestroy || function() {};
      this._onClick = options.onClick || function() {};
      this._backdrop = this._createBackdropElement();
      this._backdrop.addEventListener('click', function(event) {
        return _this._onClick(_this, event);
      });
      this._elements = [];
      this._highlightElements = {};
      this._isActive = false;
      this._onCreate(this);
    }

    Backdrop.prototype.add = function(elm) {
      var item, _i, _len;
      if (this._isEnumerable(elm)) {
        for (_i = 0, _len = elm.length; _i < _len; _i++) {
          item = elm[_i];
          this.add(item);
        }
      }
      return this._add(elm);
    };

    Backdrop.prototype._add = function(elm) {
      if (elm.tagName && this._elements.indexOf(elm) === -1) {
        if ((this._getStyle(elm, 'position')).position === 'static') {
          elm.style.position = 'relative';
        }
        this._elements.push(elm);
        this._highlightElements[this._identifyElement(elm)] = this._createHighlightElement();
        if (this._isActive) {
          this._show(elm);
        }
        return this._onAdd(this, elm);
      }
    };

    Backdrop.prototype.remove = function(elm) {
      var item, _i, _len;
      if (this._isEnumerable(elm)) {
        for (_i = 0, _len = elm.length; _i < _len; _i++) {
          item = elm[_i];
          this.remove(item);
        }
      }
      return this._remove(elm);
    };

    Backdrop.prototype._remove = function(elm) {
      var position;
      position = this._elements.indexOf(elm);
      if (position !== -1) {
        if (this._isActive) {
          this._hide(elm);
        }
        this._elements.splice(position, 1);
        this._destroyElement(this._highlightElements[this._identifyElement(elm)]);
        delete this._highlightElements[this._identifyElement(elm)];
        return this._onRemove(this, elm);
      }
    };

    Backdrop.prototype.removeAll = function() {
      var elm, _results;
      _results = [];
      while (elm = this._elements[0]) {
        _results.push(this.remove(elm));
      }
      return _results;
    };

    Backdrop.prototype.show = function() {
      var elm, _i, _len, _ref;
      _ref = this._elements;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        elm = _ref[_i];
        this._show(elm);
      }
      this._backdrop.style.display = 'block';
      return this._isActive = true;
    };

    Backdrop.prototype._show = function(elm) {
      var elmPosition, elmProps, hlElm;
      this._addClassName(elm, this._elementClass);
      hlElm = this._highlightElements[this._identifyElement(elm)];
      elmProps = this._getStyle(elm, ['width', 'height', 'z-index']);
      hlElm.style.width = parseInt(elmProps.width) + (this._padding * 2) + 'px';
      hlElm.style.height = parseInt(elmProps.height) + (this._padding * 2) + 'px';
      elmPosition = this._getPosition(elm);
      hlElm.style.left = elmPosition.left - this._padding + 'px';
      hlElm.style.top = elmPosition.top - this._padding + 'px';
      hlElm.style.display = 'block';
      elm.backdropOriginalZIndex = elmProps['z-index'];
      elm.style.zIndex = this._baseZIndex + 2;
      return this._onShow(this, elm);
    };

    Backdrop.prototype.hide = function() {
      var elm, _i, _len, _ref;
      _ref = this._elements;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        elm = _ref[_i];
        this._hide(elm);
      }
      this._backdrop.style.display = 'none';
      return this._isActive = false;
    };

    Backdrop.prototype._hide = function(elm) {
      var hlElm;
      this._removeClassName(elm, this._elementClass);
      hlElm = this._highlightElements[this._identifyElement(elm)];
      hlElm.style.display = 'none';
      elm.style.zIndex = elm.backdropOriginalZIndex;
      return this._onHide(this, elm);
    };

    Backdrop.prototype.destroy = function() {
      if (this._backdrop) {
        this.hide();
        this.removeAll();
        this._destroyElement(this._backdrop);
      }
      return this._onDestroy(this);
    };

    Backdrop.prototype._createElement = function(style, className, zIndexAdjust) {
      var elm, key, val;
      if (style == null) {
        style = {};
      }
      if (className == null) {
        className = '';
      }
      if (zIndexAdjust == null) {
        zIndexAdjust = 0;
      }
      elm = document.createElement('div');
      for (key in style) {
        val = style[key];
        elm.style[key] = val;
      }
      elm.style.zIndex = this._baseZIndex + zIndexAdjust;
      elm.className = className;
      return document.body.appendChild(elm);
    };

    Backdrop.prototype._highlightElementId = 'backdropHighlightElement';

    Backdrop.prototype._createHighlightElement = function() {
      var style;
      style = {
        position: 'absolute',
        background: '#fff',
        display: 'none'
      };
      return this._createElement(style, this._highlightClass, 1);
    };

    Backdrop.prototype._createBackdropElement = function() {
      var style;
      style = {
        position: 'fixed',
        top: '0',
        left: '0',
        bottom: '0',
        right: '0',
        opacity: '0.8',
        background: '#000',
        display: 'none'
      };
      return this._createElement(style, this._backdropClass);
    };

    Backdrop.prototype._destroyElement = function(elm) {
      if ((elm != null) && elm.parentNode) {
        return elm.parentNode.removeChild(elm);
      }
    };

    Backdrop.prototype._isEnumerable = function(content) {
      return (content != null) && typeof content !== 'string' && (content.length != null);
    };

    Backdrop.prototype._getStyle = function(elm, properties) {
      var property, result, styleObj, value, _i, _len;
      if (properties == null) {
        properties = [];
      }
      result = {};
      if (elm != null) {
        if (!this._isEnumerable(properties)) {
          properties = [properties];
        }
        styleObj = window.getComputedStyle(elm);
        for (_i = 0, _len = properties.length; _i < _len; _i++) {
          property = properties[_i];
          value = styleObj.getPropertyValue(property);
          if (value != null) {
            result[property] = value;
          }
        }
      }
      return result;
    };

    Backdrop.prototype._identifyElementCounter = 0;

    Backdrop.prototype._identifyElement = function(elm) {
      var result;
      result = elm.getAttribute('id');
      if (result == null) {
        result = 'backdropId' + this._identifyElementCounter++;
        elm.setAttribute('id', result);
      }
      return result;
    };

    Backdrop.prototype._getPosition = function(elm) {
      var result;
      result = {
        top: elm.offsetTop,
        left: elm.offsetLeft
      };
      while (elm = elm.offsetParent) {
        result.top += elm.offsetTop;
        result.left += elm.offsetLeft;
      }
      return result;
    };

    Backdrop.prototype._addClassName = function(elm, className) {
      return elm.className += " " + className;
    };

    Backdrop.prototype._removeClassName = function(elm, className) {
      var re;
      re = new RegExp("(\\s|^)" + className + "(\\s|$)", 'g');
      return elm.className = elm.className.replace(re, ' ');
    };

    return Backdrop;

  })();

  window.Backdrop = Backdrop;

}).call(this);
