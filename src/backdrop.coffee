class Backdrop
  
  constructor: (options = {}) ->
    @_backdropClass = options.backdropClass or 'backdropBackdrop'
    @_highlightClass = options.highlightClass or 'backdropHighlight'
    @_elementClass = options.elementClass or 'backdropElement'
    @_baseZIndex = options.baseZIndex or 1000
    @_padding = options.padding or 5
    @_onShow = options.onShow or ->
    @_onHide = options.onHide or ->
    @_onAdd = options.onAdd or ->
    @_onRemove = options.onRemove or ->
    @_onCreate = options.onCreate or ->
    @_onDestroy = options.onDestroy or ->
    @_onClick = options.onClick or ->
    @_backdrop = @_createBackdropElement()
    @_backdrop.addEventListener 'click', (event) => @_onClick @, event
    @_elements = []
    @_highlightElements = {}
    @_isActive = false
    @_onCreate @
  
  add: (elm) ->
    if @_isEnumerable elm then @add item for item in elm
    @_add elm
  
  _add: (elm) ->
    if elm.tagName and @_elements.indexOf(elm) is -1
      # element must have position other than static to play nice with zIndex
      if (@_getStyle elm, 'position').position is 'static'
        elm.style.position = 'relative'
      @_elements.push elm
      @_highlightElements[@_identifyElement elm] = @_createHighlightElement()
      @_show elm if @_isActive
      @_onAdd @, elm
  
  remove: (elm) ->
    if @_isEnumerable elm then @remove item for item in elm
    @_remove elm
  
  _remove: (elm) ->
    position = @_elements.indexOf elm
    if position isnt -1
      @_hide elm if @_isActive
      @_elements.splice position, 1
      @_destroyElement @_highlightElements[@_identifyElement elm]
      delete @_highlightElements[@_identifyElement elm]
      @_onRemove @, elm
  
  removeAll: ->
    @remove elm while elm = @_elements[0]
  
  show: ->
    @_show elm for elm in @_elements
    @_backdrop.style.display = 'block'
    @_isActive = true
  
  _show: (elm) ->
    @_addClassName elm, @_elementClass
    hlElm = @_highlightElements[@_identifyElement elm]
    elmProps = @_getStyle elm, ['width', 'height', 'z-index']
    hlElm.style.width = parseInt(elmProps.width) + (@_padding * 2) + 'px'
    hlElm.style.height = parseInt(elmProps.height) + (@_padding * 2) + 'px'
    elmPosition = @_getPosition elm
    hlElm.style.left = elmPosition.left - @_padding + 'px'
    hlElm.style.top = elmPosition.top - @_padding + 'px'
    hlElm.style.display = 'block'
    elm.backdropOriginalZIndex = elmProps['z-index']
    elm.style.zIndex = @_baseZIndex + 2
    @_onShow @, elm
  
  hide: ->
    @_hide elm for elm in @_elements
    @_backdrop.style.display = 'none'
    @_isActive = false
  
  _hide: (elm) ->
    @_removeClassName elm, @_elementClass
    hlElm = @_highlightElements[@_identifyElement elm]
    hlElm.style.display = 'none'
    elm.style.zIndex = elm.backdropOriginalZIndex
    @_onHide @, elm
  
  destroy: ->
    if @_backdrop
      @hide()
      @removeAll()
      @_destroyElement @_backdrop
    @_onDestroy @
  
  _createElement: (style = {}, className = '', zIndexAdjust = 0) ->
    elm = document.createElement 'div'
    elm.style[key] = val for key, val of style
    elm.style.zIndex = @_baseZIndex + zIndexAdjust
    elm.className = className
    document.body.appendChild elm
  
  _highlightElementId: 'backdropHighlightElement'
  
  _createHighlightElement: ->
    style =
      position: 'absolute'
      background: '#fff'
      display: 'none'
    @_createElement style, @_highlightClass, 1
  
  _createBackdropElement: ->
    style =
      position: 'fixed'
      top: '0'
      left: '0'
      bottom: '0'
      right: '0'
      opacity: '0.8'
      background: '#000'
      display: 'none'
    @_createElement style, @_backdropClass
  
  _destroyElement: (elm) ->
    elm.parentNode.removeChild elm if elm? and elm.parentNode
  
  _isEnumerable: (content) ->
    content? and typeof content isnt 'string' and content.length?
    
  _getStyle: (elm, properties = []) ->
    result = {}
    if elm?
      properties = [properties] if not @_isEnumerable properties
      styleObj = window.getComputedStyle elm
      for property in properties
        value = styleObj.getPropertyValue property
        result[property] = value if value?
    result
  
  _identifyElementCounter: 0
  _identifyElement: (elm) ->
    result = elm.getAttribute 'id'
    if not result?
      result = 'backdropId' + @_identifyElementCounter++
      elm.setAttribute 'id', result
    result
  
  _getPosition: (elm) ->
    result =
      top: elm.offsetTop
      left: elm.offsetLeft
    while elm = elm.offsetParent
      result.top += elm.offsetTop
      result.left += elm.offsetLeft
    result
  
  _addClassName: (elm, className) ->
    elm.className += " #{className}"
  
  _removeClassName: (elm, className) ->
    re = new RegExp "(\\s|^)#{className}(\\s|$)", 'g'
    elm.className = elm.className.replace re, ' '

# expose Backdrop object to global namespace
window.Backdrop = Backdrop
