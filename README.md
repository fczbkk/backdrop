Backdrop
========

JavaScript object that allows to darken the whole page and highlight only selected elements.
