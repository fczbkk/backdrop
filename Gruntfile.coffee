module.exports = (grunt) ->

  grunt.initConfig
    
    coffeelint:
      app: ['src/backdrop.coffee', 'test/src/backdrop.spec.coffee']
    
    jasmine:
      default:
        src: ['build/backdrop.js']
        options:
          specs: 'test/spec/backdrop.spec.js'

    coffee:
      default:
        files:
          'build/backdrop.js' : ['src/backdrop.coffee']
          'test/spec/backdrop.spec.js' : ['test/src/backdrop.spec.coffee']

    uglify:
      default:
        files:
          'build/backdrop.min.js' : ['build/backdrop.js']
    
    watch:
      default:
        files: ['src/backdrop.coffee', 'test/src/backdrop.spec.coffee']
        tasks: ['dev']
    
  grunt.loadNpmTasks 'grunt-coffeelint'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-jasmine'
  
  grunt.registerTask 'build', ['dev', 'uglify:default']
  grunt.registerTask 'dev', ['coffeelint', 'coffee:default', 'jasmine:default']
  grunt.registerTask 'default', ['dev', 'watch:default']
