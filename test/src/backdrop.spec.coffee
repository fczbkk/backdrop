createTestElement = ->
  elm = document.createElement 'div'
  elm.className = 'test'
  document.body.appendChild elm

createPositionedElement = ->
  elm = document.createElement 'div'
  elm.className = 'posTest'
  elm.style.position = 'absolute'
  elm.style.top = '100px'
  elm.style.left = '50px'
  elm.style.width = '200px'
  elm.style.height = '300px'
  document.body.appendChild elm

getHlElm = (obj, elm) ->
  obj._highlightElements[obj._identifyElement elm]

getHlStyle = (obj, elm, properties) ->
  obj._getStyle getHlElm(obj, elm), properties

getHlProperty = (obj, elm, property) ->
  result = getHlStyle obj, elm, property
  result[property]

describe 'Backdrop', ->
  
  b = null # placeholder for Backdrop object
  t = null # placeholder for collection of 10 test elements
  p = null # placeholder for absolutely positioned element
  p2 = null # placeholder for one absolutely positioned element inside another
  
  beforeEach ->
    b = new Backdrop()
    createTestElement() for i in [1..10]
    t = document.querySelectorAll '.test'
    p = createPositionedElement()
    p2 = createPositionedElement()
    p.appendChild p2
    
  afterEach ->
    b.destroy()
    elm.parentNode.removeChild elm for elm in t
    p.parentNode.removeChild p
  
  it 'should add element to be highlighted', ->
    b.add p
    expect(b._elements[0]).toBe p
  
  it 'should prevent duplicate records when adding an element', ->
    b.add p
    b.add p
    b.add p
    expect(b._elements.length).toEqual 1

  it 'should only add elements', ->
    b.add 'x'
    expect(b._elements.length).toEqual 0

  it 'should add bulk of elements to be highlighted', ->
    b.add t
    expect(b._elements.length).toEqual 10

  it 'should remove element', ->
    b.add p
    b.remove p
    expect(b._elements.length).toEqual 0

  it 'should remove bulk of elements', ->
    b.add t
    b.remove t
    expect(b._elements.length).toEqual 0
  
  it 'should remove all elements', ->
    b.add t
    b.removeAll()
    expect(b._elements.length).toEqual 0

  it 'should be able to detect collections', ->
    expect(b._isEnumerable t).toBe true
    expect(b._isEnumerable []).toBe true
    expect(b._isEnumerable p).toBe false
    expect(b._isEnumerable 'aaa').toBe false
    expect(b._isEnumerable null).toBe false

  it 'should create highlight element when element is added', ->
    b.add p
    result = b._highlightElements[b._identifyElement p]
    expect(result.tagName).toEqual 'DIV'
    expect(result.className).toEqual 'backdropHighlight'
  
  it 'should create unique highlight element for each element', ->
    b.add t
    id0 = b._identifyElement t[0]
    id1 = b._identifyElement t[1]
    b._highlightElements[id0].innerHTML = 'aaa'
    b._highlightElements[id1].innerHTML = 'bbb'
    expect(b._highlightElements[id0].innerHTML).toEqual 'aaa'
    expect(b._highlightElements[id1].innerHTML).toEqual 'bbb'
  
  it 'should remove highlighted element when element is removed', ->
    b.add p
    b.remove p
    expect(b._highlightElements[p]).toBeFalsy()
    
  it 'should be able to get style properties of an element', ->
    result = b._getStyle p, ['position', 'width', 'visibility']
    expect(result.position).toEqual 'absolute'
    expect(result.width).toEqual '200px'
    expect(result.visibility).toEqual 'visible'

  it 'should keep track of current state', ->
    expect(b._isActive).toBe false
    b.show()
    expect(b._isActive).toBe true
    b.hide()
    expect(b._isActive).toBe false

  it 'should be able to identify an element', ->
    result1 = b._identifyElement p
    result2 = b._identifyElement p
    expect(result1).toBeDefined()
    expect(result1).toEqual result2
    p.setAttribute 'id', 'aaa'
    expect(b._identifyElement p).toEqual 'aaa'
  
  it 'should show backdrop', ->
    b.show()
    result = b._getStyle b._backdrop, 'display'
    expect(result.display).toEqual 'block'
    
  it 'should hide backdrop', ->
    b.show()
    b.hide()
    result = b._getStyle b._backdrop, 'display'
    expect(result.display).toEqual 'none'

  it 'should display highlight element on show', ->
    b.add p
    b.show()
    expect(getHlProperty b, p, 'display').toEqual 'block'
  
  it 'should hide highlight element on hide', ->
    b.add p
    b.show()
    b.hide()
    expect(getHlProperty b, p, 'display').toEqual 'none'

  it 'should show highlight when element is added and backdrop is on', ->
    b.show()
    b.add p
    expect(getHlProperty b, p, 'display').toEqual 'block'

  it 'should hide highlight when element is removed and backdrop is off', ->
    b.show()
    b.hide()
    b.add p
    expect(getHlProperty b, p, 'display').toEqual 'none'

  it 'should set correct position of element', ->
    b.add p
    expect((b._getStyle p, 'position').position).toEqual 'absolute'
    b.add t[0]
    expect((b._getStyle t[0], 'position').position).toEqual 'relative'
  
  it 'should set correct size of highlight element', ->
    b.add p
    b.show()
    result = getHlStyle b, p, ['width', 'height']
    expect(result.width).toEqual '210px'
    expect(result.height).toEqual '310px'

  it 'should set correct position of highlight element', ->
    b.add p
    b.add p2
    b.show()
    result = getHlStyle b, p, ['top', 'left']
    expect(result.top).toEqual '95px'
    expect(result.left).toEqual '45px'
    result = getHlStyle b, p2, ['top', 'left']
    expect(result.top).toEqual '195px'
    expect(result.left).toEqual '95px'
  
  it 'should add classnames to backdrop, highlight and element', ->
    b.add p
    b.show()
    hlClassName = getHlElm(b, p).className
    expect(b._backdrop.className.split ' ').toContain 'backdropBackdrop'
    expect(p.className.split ' ').toContain 'backdropElement'
    expect(hlClassName.split ' ').toContain 'backdropHighlight'
    
  it 'should remove classname from element', ->
    b.add p
    b.show()
    b.hide()
    expect(p.className.split ' ').not.toContain 'backdropElement'
    
  it 'should add correct z-index to backdrop, highlight and element', ->
    b.add p
    b.show()
    expect((b._getStyle b._backdrop, 'z-index')['z-index']).toBe '1000'
    expect((b._getStyle getHlElm(b, p), 'z-index')['z-index']).toBe '1001'
    expect((b._getStyle p, 'z-index')['z-index']).toBe '1002'

  it 'should return element z-index back', ->
    p.style.zIndex = '100'
    b.add p
    b.show()
    b.hide()
    expect((b._getStyle p, 'z-index')['z-index']).toBe '100'

  # TODO rewrite this test using spies, check for return values
  it 'should support onCreate callback', ->
    testVal = 'aaa'
    options =
      onCreate: -> testVal = 'bbb'
      onAdd: -> testVal = 'ccc'
      onShow: -> testVal = 'ddd'
      onHide: -> testVal = 'eee'
      onRemove: -> testVal = 'fff'
      onDestroy: -> testVal = 'ggg'
    b = new Backdrop options
    expect(testVal).toEqual 'bbb'
    b.add p
    expect(testVal).toEqual 'ccc'
    b.show()
    expect(testVal).toEqual 'ddd'
    b.hide()
    expect(testVal).toEqual 'eee'
    b.remove p
    expect(testVal).toEqual 'fff'
    b.destroy()
    expect(testVal).toEqual 'ggg'

  it 'should cleanup after itself', ->
    b.destroy()
    expect((document.querySelectorAll '.backdropHighlight').length).toEqual 0
