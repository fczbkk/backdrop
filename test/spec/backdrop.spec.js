(function() {
  var createPositionedElement, createTestElement, getHlElm, getHlProperty, getHlStyle;

  createTestElement = function() {
    var elm;
    elm = document.createElement('div');
    elm.className = 'test';
    return document.body.appendChild(elm);
  };

  createPositionedElement = function() {
    var elm;
    elm = document.createElement('div');
    elm.className = 'posTest';
    elm.style.position = 'absolute';
    elm.style.top = '100px';
    elm.style.left = '50px';
    elm.style.width = '200px';
    elm.style.height = '300px';
    return document.body.appendChild(elm);
  };

  getHlElm = function(obj, elm) {
    return obj._highlightElements[obj._identifyElement(elm)];
  };

  getHlStyle = function(obj, elm, properties) {
    return obj._getStyle(getHlElm(obj, elm), properties);
  };

  getHlProperty = function(obj, elm, property) {
    var result;
    result = getHlStyle(obj, elm, property);
    return result[property];
  };

  describe('Backdrop', function() {
    var b, p, p2, t;
    b = null;
    t = null;
    p = null;
    p2 = null;
    beforeEach(function() {
      var i, _i;
      b = new Backdrop();
      for (i = _i = 1; _i <= 10; i = ++_i) {
        createTestElement();
      }
      t = document.querySelectorAll('.test');
      p = createPositionedElement();
      p2 = createPositionedElement();
      return p.appendChild(p2);
    });
    afterEach(function() {
      var elm, _i, _len;
      b.destroy();
      for (_i = 0, _len = t.length; _i < _len; _i++) {
        elm = t[_i];
        elm.parentNode.removeChild(elm);
      }
      return p.parentNode.removeChild(p);
    });
    it('should add element to be highlighted', function() {
      b.add(p);
      return expect(b._elements[0]).toBe(p);
    });
    it('should prevent duplicate records when adding an element', function() {
      b.add(p);
      b.add(p);
      b.add(p);
      return expect(b._elements.length).toEqual(1);
    });
    it('should only add elements', function() {
      b.add('x');
      return expect(b._elements.length).toEqual(0);
    });
    it('should add bulk of elements to be highlighted', function() {
      b.add(t);
      return expect(b._elements.length).toEqual(10);
    });
    it('should remove element', function() {
      b.add(p);
      b.remove(p);
      return expect(b._elements.length).toEqual(0);
    });
    it('should remove bulk of elements', function() {
      b.add(t);
      b.remove(t);
      return expect(b._elements.length).toEqual(0);
    });
    it('should remove all elements', function() {
      b.add(t);
      b.removeAll();
      return expect(b._elements.length).toEqual(0);
    });
    it('should be able to detect collections', function() {
      expect(b._isEnumerable(t)).toBe(true);
      expect(b._isEnumerable([])).toBe(true);
      expect(b._isEnumerable(p)).toBe(false);
      expect(b._isEnumerable('aaa')).toBe(false);
      return expect(b._isEnumerable(null)).toBe(false);
    });
    it('should create highlight element when element is added', function() {
      var result;
      b.add(p);
      result = b._highlightElements[b._identifyElement(p)];
      expect(result.tagName).toEqual('DIV');
      return expect(result.className).toEqual('backdropHighlight');
    });
    it('should create unique highlight element for each element', function() {
      var id0, id1;
      b.add(t);
      id0 = b._identifyElement(t[0]);
      id1 = b._identifyElement(t[1]);
      b._highlightElements[id0].innerHTML = 'aaa';
      b._highlightElements[id1].innerHTML = 'bbb';
      expect(b._highlightElements[id0].innerHTML).toEqual('aaa');
      return expect(b._highlightElements[id1].innerHTML).toEqual('bbb');
    });
    it('should remove highlighted element when element is removed', function() {
      b.add(p);
      b.remove(p);
      return expect(b._highlightElements[p]).toBeFalsy();
    });
    it('should be able to get style properties of an element', function() {
      var result;
      result = b._getStyle(p, ['position', 'width', 'visibility']);
      expect(result.position).toEqual('absolute');
      expect(result.width).toEqual('200px');
      return expect(result.visibility).toEqual('visible');
    });
    it('should keep track of current state', function() {
      expect(b._isActive).toBe(false);
      b.show();
      expect(b._isActive).toBe(true);
      b.hide();
      return expect(b._isActive).toBe(false);
    });
    it('should be able to identify an element', function() {
      var result1, result2;
      result1 = b._identifyElement(p);
      result2 = b._identifyElement(p);
      expect(result1).toBeDefined();
      expect(result1).toEqual(result2);
      p.setAttribute('id', 'aaa');
      return expect(b._identifyElement(p)).toEqual('aaa');
    });
    it('should show backdrop', function() {
      var result;
      b.show();
      result = b._getStyle(b._backdrop, 'display');
      return expect(result.display).toEqual('block');
    });
    it('should hide backdrop', function() {
      var result;
      b.show();
      b.hide();
      result = b._getStyle(b._backdrop, 'display');
      return expect(result.display).toEqual('none');
    });
    it('should display highlight element on show', function() {
      b.add(p);
      b.show();
      return expect(getHlProperty(b, p, 'display')).toEqual('block');
    });
    it('should hide highlight element on hide', function() {
      b.add(p);
      b.show();
      b.hide();
      return expect(getHlProperty(b, p, 'display')).toEqual('none');
    });
    it('should show highlight when element is added and backdrop is on', function() {
      b.show();
      b.add(p);
      return expect(getHlProperty(b, p, 'display')).toEqual('block');
    });
    it('should hide highlight when element is removed and backdrop is off', function() {
      b.show();
      b.hide();
      b.add(p);
      return expect(getHlProperty(b, p, 'display')).toEqual('none');
    });
    it('should set correct position of element', function() {
      b.add(p);
      expect((b._getStyle(p, 'position')).position).toEqual('absolute');
      b.add(t[0]);
      return expect((b._getStyle(t[0], 'position')).position).toEqual('relative');
    });
    it('should set correct size of highlight element', function() {
      var result;
      b.add(p);
      b.show();
      result = getHlStyle(b, p, ['width', 'height']);
      expect(result.width).toEqual('210px');
      return expect(result.height).toEqual('310px');
    });
    it('should set correct position of highlight element', function() {
      var result;
      b.add(p);
      b.add(p2);
      b.show();
      result = getHlStyle(b, p, ['top', 'left']);
      expect(result.top).toEqual('95px');
      expect(result.left).toEqual('45px');
      result = getHlStyle(b, p2, ['top', 'left']);
      expect(result.top).toEqual('195px');
      return expect(result.left).toEqual('95px');
    });
    it('should add classnames to backdrop, highlight and element', function() {
      var hlClassName;
      b.add(p);
      b.show();
      hlClassName = getHlElm(b, p).className;
      expect(b._backdrop.className.split(' ')).toContain('backdropBackdrop');
      expect(p.className.split(' ')).toContain('backdropElement');
      return expect(hlClassName.split(' ')).toContain('backdropHighlight');
    });
    it('should remove classname from element', function() {
      b.add(p);
      b.show();
      b.hide();
      return expect(p.className.split(' ')).not.toContain('backdropElement');
    });
    it('should add correct z-index to backdrop, highlight and element', function() {
      b.add(p);
      b.show();
      expect((b._getStyle(b._backdrop, 'z-index'))['z-index']).toBe('1000');
      expect((b._getStyle(getHlElm(b, p), 'z-index'))['z-index']).toBe('1001');
      return expect((b._getStyle(p, 'z-index'))['z-index']).toBe('1002');
    });
    it('should return element z-index back', function() {
      p.style.zIndex = '100';
      b.add(p);
      b.show();
      b.hide();
      return expect((b._getStyle(p, 'z-index'))['z-index']).toBe('100');
    });
    it('should support onCreate callback', function() {
      var options, testVal;
      testVal = 'aaa';
      options = {
        onCreate: function() {
          return testVal = 'bbb';
        },
        onAdd: function() {
          return testVal = 'ccc';
        },
        onShow: function() {
          return testVal = 'ddd';
        },
        onHide: function() {
          return testVal = 'eee';
        },
        onRemove: function() {
          return testVal = 'fff';
        },
        onDestroy: function() {
          return testVal = 'ggg';
        }
      };
      b = new Backdrop(options);
      expect(testVal).toEqual('bbb');
      b.add(p);
      expect(testVal).toEqual('ccc');
      b.show();
      expect(testVal).toEqual('ddd');
      b.hide();
      expect(testVal).toEqual('eee');
      b.remove(p);
      expect(testVal).toEqual('fff');
      b.destroy();
      return expect(testVal).toEqual('ggg');
    });
    return it('should cleanup after itself', function() {
      b.destroy();
      return expect((document.querySelectorAll('.backdropHighlight')).length).toEqual(0);
    });
  });

}).call(this);
